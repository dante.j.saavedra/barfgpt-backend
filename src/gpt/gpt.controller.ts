import { Body, Controller, Post } from '@nestjs/common';
import { GptService } from './gpt.service';
import { OrtographyDto } from './dtos';

// NOTA: LOS CONTROLADORES SON LOS QUE SE ENCARGAN DE RECIBIR LAS PETICIONES HTTP Y DELEGARLAS A LOS SERVICIOS

@Controller('gpt')
export class GptController {
  constructor(private readonly gptService: GptService) {}



  @Post('orthography-check')
  orthographyCheck(
    @Body() ortographyDto: OrtographyDto,
  ) {
    return this.gptService.orthographyCheck(ortographyDto);
  }
}

import { Injectable } from '@nestjs/common';
import { orthographyCheckUseCase } from './uses-cases';
import { OrtographyDto } from './dtos';
import OpenAI from 'openai';


// NOTA: LOS SERVICIOS SON LOS QUE SE ENCARGAN DE LA LÓGICA DE NEGOCIO DE LA APLICACIÓN

@Injectable()
export class GptService {

    private openai = new OpenAI({
        apiKey: process.env.OPENAI_API_KEY,
    });

    async orthographyCheck(ortographyDto: OrtographyDto) {
       return await orthographyCheckUseCase(this.openai,
        {prompt: ortographyDto.prompt});
    }
}

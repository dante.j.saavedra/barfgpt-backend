import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectConnection, InjectModel  } from '@nestjs/mongoose';
import { Model, Connection } from 'mongoose';
import { QA } from './dtos/qa.dto';
import { HNSWLib } from '@langchain/community/vectorstores/hnswlib';
import { OpenAIEmbeddings, ChatOpenAI } from '@langchain/openai';
import { RecursiveCharacterTextSplitter } from 'langchain/text_splitter';
import { formatDocumentsAsString } from 'langchain/util/document';
import { RunnablePassthrough, RunnableSequence } from '@langchain/core/runnables';
import { StringOutputParser } from "@langchain/core/output_parsers";
import { ChatPromptTemplate, HumanMessagePromptTemplate, SystemMessagePromptTemplate } from '@langchain/core/prompts';

@Injectable()
export class BarfRagService {
    private retriever: any;
    private model: any;
    private chain: any;

    constructor(
        @InjectConnection() private readonly connection: Connection,
        @InjectModel('BARF') private readonly qaModel: Model<QA>
    ) {}

    async onModuleInit() {
        const qaDocuments = await this.qaModel.find().exec();
        const docs = qaDocuments.map(doc => ({
            pageContent: `Pregunta: ${doc.Question}\nRespuesta: ${doc.Reply}`,
            pageNumber: doc._id,
            metadata: { _id: doc._id }
        }));
        const vectorStore = await HNSWLib.fromDocuments(docs, new OpenAIEmbeddings());
        this.retriever = vectorStore.asRetriever();
    
        const SYSTEM_TEMPLATE = `Utiliza los siguientes elementos de contexto para responder a la pregunta del final.
        Si no sabes la respuesta, di que no la sabes, no intentes inventarte una respuesta.
        Retorna la pregunta más parecida que tengas en tu dataset.

        ----------------
        {context}`;
        const messages = [
          SystemMessagePromptTemplate.fromTemplate(SYSTEM_TEMPLATE),
          HumanMessagePromptTemplate.fromTemplate("{question}"),
        ];
        const prompt = ChatPromptTemplate.fromMessages(messages);
        this.model = new ChatOpenAI();
        this.chain = RunnableSequence.from([
          {
            context: this.retriever.pipe(formatDocumentsAsString),
            question: new RunnablePassthrough(),
          },
          prompt,
          this.model,
          new StringOutputParser(),
        ]);

      }
    
      async getAnswerWithSources(question: string) {
        const answer = await this.chain.invoke(question);
    
        const retrievedResults = await this.retriever.getRelevantDocuments(question);
        const documents = retrievedResults.map(doc => ({
          pageContent: doc.pageContent,
          pageNumber: doc.metadata.loc?.pageNumber || 'N/A',
        }));
    
        return { answer, documents };
      }





    async listDatabases() {
        const admin = this.connection.db.admin();
        const result = await admin.listDatabases();
        return result.databases;
    }


    async listCollections(): Promise<any[]> {
        const collections = await this.connection.db.listCollections().toArray();
        console.log('Collections:', collections);
        return collections;
    }
    
    async isConnected(): Promise<boolean> {
        try {
          await this.connection.db.admin().ping();
          return true;
        } catch (error) {
          console.error('Error pinging MongoDB:', error);
          return false;
        }
    }
    
    async findAll(): Promise<QA[]> {
        return this.qaModel.find().exec();
    }

    async findOneById(id: string): Promise<QA> {
        return this.qaModel.findById(id).exec();
    }

    
}



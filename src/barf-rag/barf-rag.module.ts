import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { BarfRagService } from './barf-rag.service';
import { LangChainService } from './langchain.service';
import { QASchema } from './dtos/qa.dto';
import { BarfRagController } from './barf-rag.controller';
@Module({
    imports: [MongooseModule.forFeature([{ name: 'BARF', schema: QASchema, collection: 'dataset_libro' }]),
],
    providers: [BarfRagService, LangChainService],
    exports: [BarfRagService, LangChainService],
    controllers: [BarfRagController],
})
export class BarfRagModule {

}

import { Body, Controller, Get, Param, Query } from '@nestjs/common';
import { BarfRagService } from './barf-rag.service';
import { LangChainService } from './langchain.service';
import { QA } from './dtos/qa.dto';

@Controller('barf-rag')
export class BarfRagController {
    constructor(
        private readonly barfRagService:BarfRagService,
        private readonly langChainService:LangChainService
    
    ) {}

    @Get('databases')
    async getDatabases() {
        return this.barfRagService.listDatabases();
    }

    @Get('pregunta')
    async getHola(@Query('question') question: string): Promise<any> {
        console.log(question);
        const { answer, documents } = await this.barfRagService.getAnswerWithSources(question);
        console.log({ answer, documents });
        return { answer, documents };
    }

    @Get('answer')
    async getAnswer(@Query('question') question: string): Promise<any> {
        console.log("Hola");
        const answer = await this.langChainService.Answer(question);
        return answer;
    }
    @Get('conversacion')
    async getConversacion(
        @Query('sessionId') sessionId: string,
        @Query('question') question: string): Promise<any> {
        const answer = await this.langChainService.conversacion(sessionId, question);
        return answer;
    }
    @Get('conversacionwithRetrievalChain')
    async getConversacionwithRetrievalChain(
        @Query('sessionId') sessionId: string,
        @Query('question') question: string): Promise<any> {
        const answer = await this.langChainService.conversacionwithRetrievalChain(sessionId, question);
        return answer;
    }
    @Get('newConversation')
    async getNewConversation(@Query('username') username: string): Promise<string> {
        const answer = await this.langChainService.newConversation(username);
        return answer;
    }
    @Get('getConversationsIdByUsername')
    async getConversationsIdByUsername(@Query('username') username: string): Promise<any> {
        const answer = await this.langChainService.getConversationsIDbyUsername(username);
        return answer;
    }
    @Get('getAllMessages')
    async getAllMessages(@Query('sessionId') sessionId: string): Promise<any> {
        const answer = await this.langChainService.getallMessages(sessionId);
        return answer;
    }





    @Get('collections')
    async getCollections() {
        return this.barfRagService.listCollections();
    }

    @Get('status')
    async getStatus() {
        console.log("statusssssssss");
        const isConnected = await this.barfRagService.isConnected();
        return { isConnected };
    }

    @Get()
    async findAll(): Promise<QA[]> {
        return this.barfRagService.findAll();
    }

    @Get(':id')
    async findOneById(@Param('id') id: string): Promise<QA> {
        return this.barfRagService.findOneById(id);
    }






    
}
import { SystemMessagePromptTemplate,HumanMessagePromptTemplate  } from '@langchain/core/prompts';
import { RunnableSequence, RunnablePassthrough } from '@langchain/core/runnables';
import { ChatPromptTemplate, PromptTemplate } from '@langchain/core/prompts';
import { StringOutputParser } from '@langchain/core/output_parsers';
import { Injectable } from '@nestjs/common';
import { MongoClient, ObjectId } from "mongodb";
import { BufferMemory } from "langchain/memory";
import { InjectConnection, InjectModel  } from '@nestjs/mongoose';
import { Model, Connection } from 'mongoose';
import { QA } from './dtos/qa.dto';
import { OpenAIEmbeddings, ChatOpenAI } from "@langchain/openai";
import { ConversationChain } from "langchain/chains";
import { MongoDBChatMessageHistory } from "@langchain/mongodb";
import { MongoDBAtlasVectorSearch } from "@langchain/mongodb";
import { formatDocumentsAsString } from 'langchain/util/document';
import { AIMessage, HumanMessage } from "@langchain/core/messages";


@Injectable()
export class LangChainService {
  private openAI: ChatOpenAI;
  private clienteMongo: MongoClient;
  private retriever: any;

  constructor(
    @InjectConnection() private readonly connection: Connection,
    @InjectModel('BARF') private readonly qaModel: Model<QA>,
  ) {}

  async onModuleInit() {
    this.openAI = new ChatOpenAI({
      openAIApiKey: process.env.OPENAI_API_KEY,
      temperature: 0,
    });
    this.clienteMongo = await MongoClient.connect(process.env.MONGO_URI, {
      driverInfo: { name: 'langchainjs' },
    });
    await this.clienteMongo.connect();

    const collection_qa = this.clienteMongo
      .db('BARF')
      .collection('dataset_libro_openai');

    const vectorStore = new MongoDBAtlasVectorSearch(
      new OpenAIEmbeddings({
        apiKey: process.env.OPENAI_API_KEY,
        batchSize: 512, // Default value if omitted is 512. Max is 2048
        model: 'text-embedding-3-large',
      }),
      {
        collection: collection_qa,
        indexName: 'indice',
        textKey: 'Question',
        embeddingKey: 'Question_Embedding',
      },
    );
    this.retriever = vectorStore.asRetriever();
    //--------------------------------------------------------------------------------
  }

  async newConversation(username: string) {
    const collection_user = this.clienteMongo
      .db('BARFGPT')
      .collection('Users_Conversations');
    const sessionId = new ObjectId().toString();
    await collection_user.insertOne({ username, sessionId });
    const collection_conversations = this.clienteMongo
      .db('BARFGPT')
      .collection('Conversations');
    const memory = new BufferMemory({
      chatHistory: new MongoDBChatMessageHistory({
        collection: collection_conversations,
        sessionId,
      }),
    });

    const prompt = new ChatPromptTemplate({
      promptMessages: [
        SystemMessagePromptTemplate.fromTemplate(
          'Hola, soy un asistente virtual de YourBARF. Estoy aquí para ayudarte con cualquier duda que tengas sobre la dieta BARF. ¿En qué puedo ayudarte hoy?',
        ),
        HumanMessagePromptTemplate.fromTemplate('Mi nombre es: {name}'),
      ],
      inputVariables: ['name'],
    });
    const conversationChain = new ConversationChain({
      llm: this.openAI,
      memory,
      prompt,
    });
    await conversationChain.call({ name: username });
    return sessionId;
  }

  async Answer(question: string) {
    const stream = await this.openAI.stream(question);
    const chunks = [];
    for await (const chunk of stream) {
      chunks.push(chunk);
      console.log(`${chunk.content}|`);
    }
    return question;
  }

  async conversacion(sessionId: string, question: string) {
    const collection_conversations = this.clienteMongo
      .db('BARFGPT')
      .collection('Conversations');
    const memory = new BufferMemory({
      chatHistory: new MongoDBChatMessageHistory({
        collection: collection_conversations,
        sessionId,
      }),
    });
    const conversationChain = new ConversationChain({
      llm: this.openAI,
      memory,
    });
    const response = await conversationChain.invoke({ input: question });
    return response;
  }



  async getConversationsIDbyUsername(username: string) {
    const collection_user = this.clienteMongo
      .db('BARFGPT')
      .collection('Users_Conversations');
    const conversations = await collection_user.find({ username }).toArray();
    return conversations;
  }

  async getallMessages(sessionId: string) {
    const collection_conversations = this.clienteMongo
      .db('BARFGPT')
      .collection('Conversations');
    const memory = new BufferMemory({
      chatHistory: new MongoDBChatMessageHistory({
        collection: collection_conversations,
        sessionId,
      }),
    });
    let _messages: any = await memory.chatHistory.getMessages();

    let messages = _messages.map((message) => {
      let type = 'unknown'; // valor predeterminado por si no es HumanMessage ni AIMessage
      if (message.constructor.name === 'HumanMessage') {
        type = 'Human';
      } else if (message.constructor.name === 'AIMessage') {
        type = 'Ai';
      }

      return {
        content: message.content,
        type: type,
      };
    });

    return messages;
  }
  

  async conversacionwithRetrievalChain(sessionId: string, question: string) {
    const collection_conversations = this.clienteMongo.db('BARFGPT').collection('Conversations');
    const chatHistory = new MongoDBChatMessageHistory({collection: collection_conversations,sessionId})
    const memory = new BufferMemory({chatHistory});
    const condenseQuestionTemplate = `Given the following conversation and a follow up question, rephrase the follow up question to be a standalone question, in its original language (spanish).
    Chat History:
    {chat_history}
    Follow Up Input: {question}
    Standalone question:`;
    const CONDENSE_QUESTION_PROMPT = PromptTemplate.fromTemplate(
      condenseQuestionTemplate,
    );
    const answerTemplate = `Answer the question based only on the following context:
    {context}

    If the answer is not in context mention that you can only answer about the B.A.R.F. Diet, in its original language (spanish).

    Question: {question}`;
    const ANSWER_PROMPT = PromptTemplate.fromTemplate(answerTemplate);

    let _messages: any = await memory.chatHistory.getMessages();

    let _chat_history = [];
    for (let i = 0; i < _messages.length; i += 2) {
      _chat_history.push([_messages[i].content, _messages[i + 1].content]);
    }

    type ConversationalRetrievalQAChainInput = {
      question: string;
      chat_history: [string, string][];
    };

    const formatChatHistory = (chatHistory: [string, string][]) => {
      const formattedDialogueTurns = chatHistory.map(
        (dialogueTurn) =>
          `Human: ${dialogueTurn[0]}\nAssistant: ${dialogueTurn[1]}`,
      );
      return formattedDialogueTurns.join('\n');
    };

    const standaloneQuestionChain = RunnableSequence.from([
      {
        question: (input: ConversationalRetrievalQAChainInput) =>
          input.question,
        chat_history: (input: ConversationalRetrievalQAChainInput) =>
          formatChatHistory(input.chat_history),
      },
      CONDENSE_QUESTION_PROMPT,
      this.openAI,
      new StringOutputParser(),
    ]);

    const answerChain = RunnableSequence.from([
      {
        context: this.retriever.pipe(formatDocumentsAsString),
        question: new RunnablePassthrough(),
      },
      ANSWER_PROMPT,
      this.openAI,
    ]);

    const conversationalRetrievalQAChain = standaloneQuestionChain.pipe(answerChain);
    const result = await conversationalRetrievalQAChain.invoke({
      question,
      chat_history: _chat_history,
    });
    const { content } = result;
    chatHistory.addMessage(new HumanMessage(question));
    chatHistory.addMessage(new AIMessage(content.toString()));
    const contexto = await this.retriever.invoke(question)
    return content;

  }

}
import { Schema, Document } from 'mongoose';

export interface QA extends Document {
  Question: string;
  Reply: string;
  Question_embedding: number[];
  Reply_embedding: number[];
  _id: string;
}

export const QASchema = new Schema({
  
  Question: { type: String, required: true },
  Reply: { type: String, required: true },
  Question_embedding: { type: [Number], required: true },
  Reply_embedding: { type: [Number], required: true },
  _id: { type: String, required: true },
});
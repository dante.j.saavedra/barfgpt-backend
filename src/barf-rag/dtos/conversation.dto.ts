export interface conversationsDto {
    lc:     number;
    type:   string;
    id:     string[];
    kwargs: Kwargs;
}

export interface Kwargs {
    content:             string;
    additional_kwargs:   AdditionalKwargs;
    response_metadata:   ResponseMetaData;
    tool_calls?:         any[];
    invalid_tool_calls?: any[];
}

export interface AdditionalKwargs {
}

export interface ResponseMetaData {
}

import { Body, Controller, Get, Post } from '@nestjs/common';
import { BarfAssistantService } from './barf-assistant.service';
import { QuestionDto } from './dtos/question.dto';

@Controller('barf-assistant')
export class BarfAssistantController {
  constructor(private readonly barfAssistantService: BarfAssistantService) {}


    @Post('create-thread')
    async createThread() {
      return this.barfAssistantService.createThread();
    }

    @Post('user-question')
    async userQuestion(
      @Body() questionDto: QuestionDto
    ) {
      return this.barfAssistantService.userQuestion(questionDto);
    }

    @Get('hola')
    async hola() {
      return 'hola';
    }
}

import { QuestionDto } from './dtos/question.dto';
import { Injectable } from '@nestjs/common';
import OpenAI from 'openai';
import { createThreadUseCase, createMessageUseCase, createRunUseCase, checkCompleteStatusUseCase, getMessageListUseCase } from './use-cases/use-cases';


@Injectable()
export class BarfAssistantService {
    private openai = new OpenAI({
        apiKey: process.env.OPENAI_API_KEY,
    });
    
    async createThread(){
       return await createThreadUseCase(this.openai);
    }

    async userQuestion(questionDto:QuestionDto){
        const { threadID, question } = questionDto;
        const message = await createMessageUseCase(this.openai, { threadID, question });
        const run = await createRunUseCase(this.openai, { threadID });
        await checkCompleteStatusUseCase(this.openai, { runID: run.id, threadID: threadID});
        const messages = await getMessageListUseCase(this.openai, { threadID });
        return messages;
    }

    



}

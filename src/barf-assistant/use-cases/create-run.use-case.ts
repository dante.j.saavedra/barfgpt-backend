import OpenAI from "openai";

interface Options{
    threadID: string;
    assistantID?: string;
}


export const createRunUseCase  = async(openai: OpenAI, options: Options) => {

    const {threadID, assistantID = process.env.ASSISTANT_ID } = options;

    const run = await openai.beta.threads.runs.create(threadID, {
        assistant_id: assistantID,
        // instructions: SI SE UTILIZA ESTA OPCIÓN SE REESCRIBE LA INSTRUCCIÓN DEL ASISTENTE
        
    });

    return run;
}

import OpenAI from "openai";

interface Options{
    threadID: string;
    runID: string;
}

export const checkCompleteStatusUseCase = async (openai: OpenAI, options: Options) => {
    const {threadID, runID} = options;
    const runStatus = await openai.beta.threads.runs.retrieve(threadID, runID);

    if(runStatus.status === 'completed'){
        return runStatus;
    }
    await new Promise(resolve => setTimeout(resolve, 1000));
    return await checkCompleteStatusUseCase(openai, options);
}
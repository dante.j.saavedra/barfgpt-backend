import OpenAI from "openai";

interface Options{
    threadID: string;
    question: string;
}


export const createMessageUseCase = async (openai: OpenAI, options: Options ) => {

    const {threadID, question} = options;

    const message  = await openai.beta.threads.messages.create(threadID, {
        role: 'user',
        content: question
    });

    return message;

}
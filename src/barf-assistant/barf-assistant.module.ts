import { Module } from '@nestjs/common';
import { BarfAssistantService } from './barf-assistant.service';
import { BarfAssistantController } from './barf-assistant.controller';

@Module({
  controllers: [BarfAssistantController],
  providers: [BarfAssistantService],
})
export class BarfAssistantModule {}

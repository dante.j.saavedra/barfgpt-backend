import { Module } from '@nestjs/common';
import { GptModule } from './gpt/gpt.module';
import { ConfigModule } from '@nestjs/config';
import { BarfAssistantModule } from './barf-assistant/barf-assistant.module';
import { BarfRagModule } from './barf-rag/barf-rag.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [GptModule,
    ConfigModule.forRoot({
     
    }),
    MongooseModule.forRoot(process.env.MONGO_URI),
    BarfAssistantModule,
    BarfRagModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
